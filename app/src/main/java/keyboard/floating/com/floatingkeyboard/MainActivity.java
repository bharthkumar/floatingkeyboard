package keyboard.floating.com.floatingkeyboard;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private Button btnClick, btnStop;
    private ToggleButton tglBoot;
    private static final int OVERLAY_PERMISSION_REQ_CODE = 1000;
    private SharedPreferences sharedpreferences;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnClick = (Button) findViewById(R.id.button);
        btnStop = (Button) findViewById(R.id.btn_stop);
        tglBoot = (ToggleButton) findViewById(R.id.toggle_boot);

        sharedpreferences = this.getSharedPreferences(FloatingConstants.KEY_PREF_NAME, Context.MODE_PRIVATE);
        tglBoot.setChecked(sharedpreferences.getBoolean(FloatingConstants.KEY_PREF_BOOT, false));

        askForOverLay();

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(MainActivity.this, FloatingViewService.class));
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(getApplication(), FloatingViewService.class));
            }
        });

        tglBoot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedpreferences.edit().putBoolean(FloatingConstants.KEY_PREF_BOOT, b).apply();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askForOverLay() {
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                // SYSTEM_ALERT_WINDOW permission not granted...
            }
        }
    }
}
