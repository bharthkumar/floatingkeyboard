package keyboard.floating.com.floatingkeyboard;

/**
 * Created by bHARATH on 4/12/16.
 */

public class FloatingConstants {

    public static final String KEY_PREF_NAME = "KEY_FLOATING_PREF";
    public static final String KEY_PREF_BOOT = "KEY_FLOATING_BOOT";
}
