package keyboard.floating.com.floatingkeyboard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by bHARATH on 4/12/16.
 */

public class FloatingKeyboardSwitcherReceiver extends BroadcastReceiver {
    private static final String TAG = FloatingKeyboardSwitcherReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "---- Boot completed received ------");
        SharedPreferences sharedPreferences = context.getSharedPreferences(FloatingConstants.KEY_PREF_NAME, context.MODE_PRIVATE);
        Log.v(TAG, "---- Boot completed received Preference value ------" + sharedPreferences.getBoolean(FloatingConstants.KEY_PREF_BOOT, false));
        if (sharedPreferences != null && sharedPreferences.getBoolean(FloatingConstants.KEY_PREF_BOOT, false)) {
            Intent in = new Intent(context, FloatingViewService.class);
            if (in != null) {
                Log.v(TAG, "---- Starting service  ------");
                context.startService(in);
            }
        }
    }
}
